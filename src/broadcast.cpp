#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <turtlesim/Pose.h>

int main(int argc, char** argv){
  ros::init(argc, argv, "local_map_tf");

  ros::NodeHandle nh;

  static tf::TransformBroadcaster br;
  tf::Transform transform;
  transform.setOrigin( tf::Vector3( 516277.6f,5064852.3f, 0.0) );
  tf::Quaternion q;
  q.setRPY(0, 0, 0);
  transform.setRotation(q);

  // Construct a map of strings
  std::vector<double> map_d(4);
  map_d.push_back(1);
  map_d.push_back(2);
  map_d.push_back(3);
  map_d.push_back(4);

  // Set and get a map of double
  nh.setParam("my_double_list", map_d);

  // Sum a list of doubles from the parameter server
  std::vector<double> my_double_list;
  double sum = 0;
  nh.getParam("my_double_list", my_double_list);

  for(unsigned i=0; i < my_double_list.size(); i++) {
    sum += my_double_list[i];
  }

  ROS_INFO_STREAM("sum: " << sum );
//	while (ros::ok())
//	{
//	  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "map", "local_map"));
//      std::cout << transform.getOrigin().getX() << std::endl;
//	  ros::Duration(0.1).sleep();
//	  ros::spinOnce();
//	}

  return 0;
}
