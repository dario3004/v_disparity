
#include <algorithm>
#include <ros/ros.h>
#include "stereo_msgs/DisparityImage.h"
#include "sensor_msgs/CameraInfo.h"
#include "sensor_msgs/Image.h"
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>     //make sure to include the relevant headerfiles
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include <cv_bridge/cv_bridge.h>
#include <math.h>
#include <iostream>
#include <Eigen/Eigen>
using namespace Eigen;
using namespace std;

// publisher
ros::Publisher publisher;

int main(int argc, char *argv[])
{
    // init ROS and NodeHandle
    ros::init(argc, argv, "homography_node");
    ros::NodeHandle node_handle("~");

    double const h =  1.77141;//1.83426;
    double const theta =  0.0297535;//0.0291128;

    // homogeneous rotation matrix on X axis by -90° --------------------------------------------------
    cv::Mat R_x = cv::Mat::eye(4, 4, CV_32F);
    double rot = - CV_PI / 2;
    R_x.at<float>(1,1) = cos(rot);
    R_x.at<float>(1,2) = -sin(rot);
    R_x.at<float>(2,1) = sin(rot);
    R_x.at<float>(2,2) = cos(rot);

    // homogeneous rotation matrix on X axis by -theta --------------------------------------------------
    cv::Mat R_theta = cv::Mat::eye(4, 4, CV_32F);
    R_theta.at<float>(1,1) = cos(-theta);
    R_theta.at<float>(1,2) = -sin(-theta);
    R_theta.at<float>(2,1) = sin(-theta);
    R_theta.at<float>(2,2) = cos(-theta);

    // homogeneous rotation matrix on Y axis by 90° ------------------------------------------------------
    cv::Mat R_y = cv::Mat::eye(4, 4, CV_32F);
    rot = CV_PI / 2;
    R_y.at<float>(0,0) = cos(rot);
    R_y.at<float>(0,2) = sin(rot);
    R_y.at<float>(2,0) = -sin(rot);
    R_y.at<float>(2,2) = cos(rot);

    // traslation matrix ---------------------------------------------------------------------------------
    cv::Mat T_h = cv::Mat::eye(4, 4, CV_32F);
    T_h.at<float>(2,3) = h;

    // Rotation matrix
    cv::Mat R = cv::Mat::zeros(4, 4, CV_32FC1);
    R = R_x * R_y * R_theta;

    // Rototraslation matrix
    cv::Mat RT = cv::Mat::zeros(4, 4, CV_32F);
    RT = T_h * R;

    std::cout << "R_x: " << std::endl;
    std::cout << R_x << std::endl << std::endl;

    std::cout << "R_y: " << std::endl;
    std::cout << R_y << std::endl << std::endl;

    std::cout << "R_theta: " << std::endl;
    std::cout << R_theta << std::endl << std::endl;

    std::cout << "R: " << std::endl;
    std::cout << R << std::endl << std::endl;

    std::cout << "T_h: " << std::endl;
    std::cout << T_h << std::endl << std::endl;

    // projection matrix (KITTI RIGHT CAMERA) ------------------------------------------------------------
    cv::Mat P = cv::Mat::zeros(3, 4, CV_32F);
    P.at<float>(0,0) = 718.856;
    P.at<float>(0,2) = 607.1928;
    P.at<float>(0,3) = -386.1448;
    P.at<float>(1,1) = 718.856;
    P.at<float>(1,2) = 185.2157;
    P.at<float>(2,2) = 1;
    std::cout << "P: " << std::endl;
    std::cout << P << std::endl << std::endl;

    // define 3d points ------------------------------------------------------------------------
    cv::Mat points_3d = (cv::Mat_<float>(4,4) <<
                         8,  8, 40, 40,
                         8, -8, -8,  8,
                         0,  0,  0,  0,
                         1,  1,  1,  1);

    // project 3d points into 2d plane
    cv::Mat points_2d = P * RT.inv() * points_3d;

    // normalize 2d points
    for(int i = 0; i < 4; ++i){
        points_2d.col(i) /= points_2d.at<float>(2,i);
    }

    // init points for findHomography
    cv::Mat image_points = cv::Mat::zeros(1, 4, CV_32FC2);
    cv::Mat world_points = cv::Mat::zeros(1, 4, CV_32FC2);

    double homography_x_resolution = 600; // In pixels
    double homography_y_resolution = 600; // In pixels
    cv::Mat points_warped = (cv::Mat_<float>(2,4) <<
                                        0, homography_x_resolution,
                                        homography_x_resolution, 0,
                                        homography_y_resolution, homography_y_resolution,
                                        0, 0);

    for(int i = 0; i < 4; ++i)
    {
        image_points.at<cv::Vec2f>(0,i)[0] = points_2d.at<float>(0,i);
        image_points.at<cv::Vec2f>(0,i)[1] = points_2d.at<float>(1,i);

        world_points.at<cv::Vec2f>(0,i)[0] = points_warped.at<float>(0,i);
        world_points.at<cv::Vec2f>(0,i)[1] = points_warped.at<float>(1,i);
    }

    // Find Homography
    cv::Mat H = cv::findHomography(image_points, world_points);

    // Init images
    cv::Mat warp_image;
    cv::Mat original_image = cv::imread("/home/dario/workspace_ros/src/v_disparity/media/right_camera.png", cv::IMREAD_GRAYSCALE);

    // Warp perspective
    cv::warpPerspective(original_image,
                        warp_image,
                        H,
                        cvSize(homography_x_resolution, homography_y_resolution),
                        cv::INTER_NEAREST);

    std::cout << "points 3d" << std::endl;
    std::cout << points_3d << std::endl << std::endl;

    std::cout << "world_pts_for_img_hom" << std::endl;
    std::cout << world_points << std::endl << std::endl;

    std::cout << "image_pts_for_img_hom" << std::endl;
    std::cout << image_points << std::endl << std::endl;

    cvNamedWindow("RightImage");
    cvNamedWindow("RightImageUnwarped");
    cvStartWindowThread();
    cv::imshow("RightImage", original_image);
    cv::imshow("RightImageUnwarped", warp_image);

//    // get homography matrix
//    cv::Mat H = cv::findHomography(srcPoints, dstPoints, CV_RANSAC);
//    H.convertTo(H, CV_32FC1);
//    cout << "H:"  << endl; cout << H << endl << endl;

//    // get transformation matrix
//    cv::Mat Lambda = cv::getPerspectiveTransform(srcPoints, dstPoints);
//    Lambda.convertTo(Lambda, CV_32FC1);
//    cout << "Lambda:" << endl; cout << Lambda << endl << endl;

//    // read image and return a grayscale cv::Mat
//    cv::Mat image = cv::imread("/home/limongi/workspace_ros/src/v_disparity/media/right_camera.png", cv::IMREAD_GRAYSCALE);
//    cv::Mat warped_image = cv::Mat::zeros(600, 600, CV_8UC1);
//    cv::Mat image_perspective = cv::Mat::zeros(600, 600, CV_8UC1);

//    // warp images
//    cv::warpPerspective(image, warped_image, H, warped_image.size());
//    cv::warpPerspective(image, image_perspective, Lambda, image_perspective.size());

//    // show images
//    cv::namedWindow( "Image", cv::WINDOW_AUTOSIZE );// Create a window for display.
//    cv::imshow( "Image", image );
//    cv::namedWindow( "Warped Image", cv::WINDOW_AUTOSIZE );// Create a window for display.
//    cv::imshow( "Warped Image", warped_image );
//    cv::namedWindow( "Image Perspective", cv::WINDOW_AUTOSIZE );// Create a window for display.
//    cv::imshow( "Image Perspective", image_perspective );
//    cv::waitKey(0);



    // spin
    ros::spin();

    return 0;
}
