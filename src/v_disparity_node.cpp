#include <algorithm>
#include <boost/filesystem.hpp>
#include <boost/array.hpp>
#include <image_transport/image_transport.h>
#include <ros/ros.h>
#include "stereo_msgs/DisparityImage.h"
#include "sensor_msgs/CameraInfo.h"
#include "sensor_msgs/Image.h"
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <ros/ros.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <cv_bridge/cv_bridge.h>
#include "visualization_msgs/Marker.h"
#include <tf/transform_broadcaster.h>
#include <math.h>

#include <dynamic_reconfigure/server.h>
#include <v_disparity/v_disparity_nodeConfig.h>


struct Line
{
  ///start point
  cv::Point2d startPoint;
  ///end point
  cv::Point2d endPoint;
  ///score of line
  float score;
};

// v_disparity results
double h;
double theta;

// publishers
ros::Publisher publisher;
ros::Publisher publisher_warp;

// camera calibration
sensor_msgs::CameraInfo::ConstPtr camera_calibration;

// image_transport vars
cv_bridge::CvImagePtr cv_ptr(new cv_bridge::CvImage);
boost::mutex cv_ptr_lock;
cv_bridge::CvImagePtr disparity_cv_ptr(new cv_bridge::CvImage);
boost::mutex disparity_ptr_lock;

// Reconfigure parameters
int disp_start_row = 0;
int disp_end_row = 0;
int disp_start_col = 0;
int disp_end_col = 0;
int min_v_disparity = 0;
int blur_size = 7;
int adaptive_block_size = 11;

int erosion_size = 1;
int erosion_element = 0; //MORPH_RECT=0, MORPH_CROSS=1, MORPH_ELLIPSE=2

int dilate_size = 1;
int dilate_element = 0; //MORPH_RECT=0, MORPH_CROSS=1, MORPH_ELLIPSE=2

double canny_threshold1 = 50;
double canny_threshold2 = 200;
int canny_aperture_size = 3;

// cv::HoughLinesP(v_disp_data, lines, 1, CV_PI/180, 80, 50, 10);
double hough_rho = 1;
double hough_theta = CV_PI/180;
int hough_threshold = 80;
double hough_min_line_lenght = 50;
double hough_max_line_gap = 10;

bool perform_blur = true;
bool perform_binarization = false;
bool perform_adaptive_binarization = false;
bool perform_erosion = false;
bool perform_dilate = false;
bool perform_canny = false;

double adaptive_c = 1;
double binarization_threshold;
//double v_disparity_confidence = 0.1;

double warping_lateral_distance = 7; // In meters!!
double warping_forward_min_distance = 4; // In meters!!
double warping_forward_max_distance = 40; // In meters!!

double homography_x_resolution = 500; // In pixels
double homography_y_resolution = 700; // In pixels

// Road lane detector
double lane_width_m = 0.0;
double lane_height_m = 0.0;
int kernel_width_px = 0;
int kernel_height_px = 0;

// Road lane detector image-processing
bool perform_lane_normalization = false;
bool perform_lane_blur = false;
bool perform_lane_quantile_thresholding = false;
bool perform_filter_negative_values = false;
bool perform_lane_binarization = false;
bool perform_smooth_sum_lines = false;
bool perform_lane_hough = false;
int smooth_sum_lines_size = 21;
double lane_binarization_threshold = 20;
int lane_blur_size = 7;
double lane_thresholding_quantile = 0.975;
double lane_min_angle = 0;
double lane_max_angle = 75;
int lane_local_max_ignore = 0;
double lane_detection_threshold = 0;
double lane_score_threshold = 0;
int lane_group_threshold = 15;
double lane_group_overlap_threshold = 0.5;
int lane_ransac_window = 15;

using namespace std;

/** ***************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************** */


/** This function checks if the given point is inside the bounding box
 * specified
 *
 * \param inLine the input line
 * \param bbox the bounding box
 * \param outLine the output line
 *
 */
bool mcvIsPointInside(CvPoint2D32f point, CvSize bbox)
{
  return (point.x>=0 && point.x<=bbox.width
      && point.y>=0 && point.y<=bbox.height) ? true : false;
}

/** This function intersects the input line (given in r and theta) with
 *  the given bounding box where the line is represented by:
 *  x cos(theta) + y sin(theta) = r
 *
 * \param r the r value for the input line
 * \param theta the theta value for the input line
 * \param bbox the bounding box
 * \param outLine the output line
 *
 */
void mcvIntersectLineRThetaWithBB(float r, float theta, const CvSize bbox,
                                  Line *outLine)
{
  //hold parameters
  double xup, xdown, yleft, yright;

  //intersect with top and bottom borders: y=0 and y=bbox.height-1
  if (cos(theta)==0) //horizontal line
  {
    xup = xdown = bbox.width+2;
  }
  else
  {
    xup = r / cos(theta);
    xdown = (r-bbox.height*sin(theta))/cos(theta);
  }

  //intersect with left and right borders: x=0 and x=bbox.widht-1
  if (sin(theta)==0) //horizontal line
  {
    yleft = yright = bbox.height+2;
  }
  else
  {
    yleft = r/sin(theta);
    yright = (r-bbox.width*cos(theta))/sin(theta);
  }

  //points of intersection
  CvPoint2D32f pts[4] = {{xup, 0},{xdown,bbox.height},
        {0, yleft},{bbox.width, yright}};

  //get the starting point
  int i;
  for (i=0; i<4; i++)
  {
    //if point inside, then put it
    if(mcvIsPointInside(pts[i], bbox))
    {
        outLine->startPoint.x = pts[i].x;
        outLine->startPoint.y = pts[i].y;
        //get out of for loop
        break;
    }
  }
  //get the ending point
  for (i++; i<4; i++)
  {
    //if point inside, then put it
    if(mcvIsPointInside(pts[i], bbox))
    {
        outLine->endPoint.x = pts[i].x;
        outLine->endPoint.y = pts[i].y;
        //get out of for loop
        break;
    }
  }
}

/** This functions fits a line using the orthogonal distance to the line
    by minimizing the sum of squares of this distance.

 *
 * \param points the input points to fit the line to which is
 *    2xN matrix with x values on first row and y values on second
 * \param lineRTheta the return line [r, theta] where the line is
 *    x*cos(theta)+y*sin(theta)=r
 * \param lineAbc the return line in [a, b, c] where the line is
 *    a*x+b*y+c=0
 *
 */
void mcvFitRobustLine(const CvMat *points, float *lineRTheta,
                      float *lineAbc)
{
  //clone the points
  CvMat *cpoints = cvCloneMat(points);
  //get mean of the points and subtract from the original points
  float meanX=0, meanY=0;
  CvScalar mean;
  CvMat row1, row2;
  //get first row, compute avg and store
  cvGetRow(cpoints, &row1, 0);
  mean = cvAvg(&row1);
  meanX = (float) mean.val[0];
  cvSubS(&row1, mean, &row1);
  //same for second row
  cvGetRow(cpoints, &row2, 1);
  mean = cvAvg(&row2);
  meanY = (float) mean.val[0];
  cvSubS(&row2, mean, &row2);

  //compute the SVD for the centered points array
  CvMat *W = cvCreateMat(2, 1, CV_32FC1);
  CvMat *V = cvCreateMat(2, 2, CV_32FC1);
  //    CvMat *V = cvCreateMat(2, 2, CV_32fC1);
  CvMat *cpointst = cvCreateMat(cpoints->cols, cpoints->rows, CV_32FC1);
  cvTranspose(cpoints, cpointst);
  cvSVD(cpointst, W, 0, V, CV_SVD_V_T);
  cvTranspose(V, V);
  cvReleaseMat(&cpointst);

  //get the [a,b] which is the second column corresponding to
  //smaller singular value
  float a, b, c;
  a = CV_MAT_ELEM(*V, float, 0, 1);
  b = CV_MAT_ELEM(*V, float, 1, 1);

  //c = -meanX*a-meanY*b
  c = -(meanX * a + meanY * b);

  //compute r and theta
  //theta = atan(b/a)
  //r = meanX cos(theta) + meanY sin(theta)
  float r, theta;
  theta = atan2(b, a);
  r = meanX * cos(theta) + meanY * sin(theta);
  //correct
  if (r<0)
  {
    //correct r
    r = -r;
    //correct theta
    theta += CV_PI;
    if (theta>CV_PI)
      theta -= 2*CV_PI;
  }
  //return
  if (lineRTheta)
  {
    lineRTheta[0] = r;
    lineRTheta[1] = theta;
  }
  if (lineAbc)
  {
    lineAbc[0] = a;
    lineAbc[1] = b;
    lineAbc[2] = c;
  }
  //clear
  cvReleaseMat(&cpoints);
  cvReleaseMat(&W);
  cvReleaseMat(&V);
}

/** This function samples uniformly with weights
 *
 * \param cumSum cumulative sum for normalized weights for the differnet
 *          samples (last is 1)
 * \param numSamples the number of samples
 * \param randInd a 1XnumSamples of int containing the indices
 * \param rng a pointer to a random number generator
 *
 */
void mcvSampleWeighted(const CvMat *cumSum, int numSamples, CvMat *randInd,
                       CvRNG *rng)
{
//     //get cumulative sum of the weights
//     //OPTIMIZE:should pass it later instead of recomputing it
//     CvMat *cumSum = cvCloneMat(weights);
//     for (int i=1; i<weights->cols; i++)
// 	CV_MAT_ELEM(*cumSum, float, 0, i) += CV_MAT_ELEM(*cumSum, float, 0, i-1);

  //check if numSamples is equal or more
  int i=0;
  if (numSamples >= cumSum->cols)
  {
    for (; i<numSamples; i++)
      CV_MAT_ELEM(*randInd, int, i, 0) = i;
  }
  else
  {
    //loop
    while(i<numSamples)
    {
      //get random number
      double r = cvRandReal(rng);

      //get the index from cumSum
      int j;
      for (j=0; j<cumSum->cols && r>CV_MAT_ELEM(*cumSum, float, 0, j); j++);

      //make sure this index wasnt chosen before
      bool put = true;
      for (int k=0; k<i; k++)
        if (CV_MAT_ELEM(*randInd, int, k, 0) == j)
          //put it
          put = false;

      if (put)
      {
        //put it in array
        CV_MAT_ELEM(*randInd, int, i, 0) = j;
        //inc
        i++;
      }
    } //while
  } //if
}

/** This function computes the cumulative sum for a vector
 *
 * \param inMat input matrix
 * \param outMat output matrix
 *
 */
void mcvCumSum(const CvMat *inMat, CvMat *outMat)
{

#define MCV_CUM_SUM(type) 				\
    /*row vector*/ 					\
    if(inMat->rows == 1) 				\
    for (int i=1; i<outMat->cols; i++) 		\
        CV_MAT_ELEM(*outMat, type, 0, i) += 	\
        CV_MAT_ELEM(*outMat, type, 0, i-1); 	\
    /*column vector*/					\
    else						\
    for (int i=1; i<outMat->rows; i++) 		\
        CV_MAT_ELEM(*outMat, type, i, 0) += 	\
        CV_MAT_ELEM(*outMat, type, i-1, 0);

  //copy to output if not equal
  if(inMat != outMat)
    cvCopy(inMat, outMat);

  //check type
  if (CV_MAT_TYPE(inMat->type)==CV_32FC1)
  {
    MCV_CUM_SUM(float)
  }
  else if (CV_MAT_TYPE(inMat->type)==CV_32SC1)
  {
    MCV_CUM_SUM(int)
  }
  else
  {
    cerr << "Unsupported type in mcvCumSum\n";
    exit(1);
  }
}

/** This function gets the indices of the non-zero values in a matrix
 *
 * \param inMat the input matrix
 * \param outMat the output matrix, with 2xN containing the x and y in
 *    each column and the pixels value [xs; ys; pixel values]
 * \param floatMat whether to return floating points or integers for
 *    the outMat
 */
CvMat* mcvGetNonZeroPoints(const CvMat *inMat, bool floatMat)
{


#define MCV_GET_NZ_POINTS(inMatType, outMatType) \
     /*loop and allocate the points*/ \
     for (int i=0; i<inMat->rows; i++) \
    for (int j=0; j<inMat->cols; j++) \
        if (CV_MAT_ELEM(*inMat, inMatType, i, j)) \
        { \
        CV_MAT_ELEM(*outMat, outMatType, 0, k) = j; \
        CV_MAT_ELEM(*outMat, outMatType, 1, k) = i; \
                CV_MAT_ELEM(*outMat, outMatType, 2, k) = \
                  (outMatType) CV_MAT_ELEM(*inMat, inMatType, i, j); \
                k++; \
        } \

  int k=0;

  //get number of non-zero points
  int numnz = cvCountNonZero(inMat);

  //allocate the point array and get the points
  CvMat* outMat;
  if (numnz)
  {
    if (floatMat)
      outMat = cvCreateMat(3, numnz, CV_32FC1);
    else
      outMat = cvCreateMat(3, numnz, CV_32SC1);
  }
  else
    return NULL;

  //check type
  if (CV_MAT_TYPE(inMat->type)==CV_32FC1 &&
    CV_MAT_TYPE(outMat->type)==CV_32FC1)
  {
    MCV_GET_NZ_POINTS(float, float)
  }
  else if (CV_MAT_TYPE(inMat->type)==CV_32FC1 &&
    CV_MAT_TYPE(outMat->type)==CV_8UC1)
  {
    MCV_GET_NZ_POINTS(float, unsigned char)
  }
  else if (CV_MAT_TYPE(inMat->type)==CV_8UC1 &&
    CV_MAT_TYPE(outMat->type)==CV_32FC1)
  {
    MCV_GET_NZ_POINTS(unsigned char, float)
  }
  else if (CV_MAT_TYPE(inMat->type)==CV_8UC1 &&
    CV_MAT_TYPE(outMat->type)==CV_8UC1)
  {
    MCV_GET_NZ_POINTS(unsigned char, unsigned char)
  }
  else
  {
    cerr << "Unsupported type in mcvGetMatLocalMax\n";
    exit(1);
  }

  //return
  return outMat;
}

/** This functions implements RANSAC algorithm for line fitting
 *   given an image
 *
 *
 * \param image input image
 * \param numSamples number of samples to take every iteration
 * \param numIterations number of iterations to run
 * \param threshold threshold to use to assess a point as a good fit to a line
 * \param numGoodFit number of points close enough to say there's a good fit
 * \param getEndPoints whether to get the end points of the line from the data,
 *  just intersect with the image boundaries
 * \param lineType the type of line to look for (affects getEndPoints)
 * \param lineXY the fitted line
 * \param lineRTheta the fitted line [r; theta]
 * \param lineScore the score of the line detected
 *
 */
void mcvFitRansacLine(const CvMat *image, int numSamples, int numIterations,
                      float threshold, float scoreThreshold, int numGoodFit,
                      bool getEndPoints,
                      Line *lineXY, float *lineRTheta, float *lineScore)
{

  //get the points with non-zero pixels
  CvMat *points;
  points = mcvGetNonZeroPoints(image,true);
  if (!points)
    return;
  //check numSamples
  if (numSamples>points->cols)
    numSamples = points->cols;
  //subtract half
  cvAddS(points, cvRealScalar(0.5), points);

  //normalize pixels values to get weights of each non-zero point
  //get third row of points containing the pixel values
  CvMat w;
  cvGetRow(points, &w, 2);
  //normalize it
  CvMat *weights = cvCloneMat(&w);
  cvNormalize(weights, weights, 1, 0, CV_L1);
  //get cumulative    sum
  mcvCumSum(weights, weights);

  //random number generator
  CvRNG rng = cvRNG(0xffffffff);
  //matrix to hold random sample
  CvMat *randInd = cvCreateMat(numSamples, 1, CV_32SC1);
  CvMat *samplePoints = cvCreateMat(2, numSamples, CV_32FC1);
  //flag for points currently included in the set
  CvMat *pointIn = cvCreateMat(1, points->cols, CV_8SC1);
  //returned lines
  float curLineRTheta[2], curLineAbc[3];
  float bestLineRTheta[2]={-1.f,0.f}, bestLineAbc[3];
  float bestScore=0, bestDist=1e5;
  float dist, score;
  Line curEndPointLine; /* ={{-1.,-1.},{-1.,-1.}}, */
  curEndPointLine.startPoint.x = -1.0;
  curEndPointLine.startPoint.y = -1.0;
  curEndPointLine.endPoint.x = -1.0;
  curEndPointLine.endPoint.y = -1.0;
  Line bestEndPointLine; // ={{-1.,-1.},{-1.,-1.}};
  bestEndPointLine.startPoint.x = -1.0;
  bestEndPointLine.startPoint.y = -1.0;
  bestEndPointLine.endPoint.x = -1.0;
  bestEndPointLine.endPoint.y = -1.0;

  //variabels for getting endpoints
  //int mini, maxi;
  float minc=1e5f, maxc=-1e5f, mind, maxd;
  float x, y, c=0.;
  CvPoint2D32f minp={-1., -1.}, maxp={-1., -1.};
  //outer loop
  for (int i=0; i<numIterations; i++)
  {
    //set flag to zero
    //cvSet(pointIn, cvRealScalar(0));
    cvSetZero(pointIn);
    //get random sample from the points
    // 	cvRandArr(&rng, randInd, CV_RAND_UNI, cvRealScalar(0), cvRealScalar(points->cols));
    mcvSampleWeighted(weights, numSamples, randInd, &rng);

    for (int j=0; j<numSamples; j++)
    {
      //flag it as included
      CV_MAT_ELEM(*pointIn, char, 0, CV_MAT_ELEM(*randInd, int, j, 0)) = 1;
      //put point
      CV_MAT_ELEM(*samplePoints, float, 0, j) =
      CV_MAT_ELEM(*points, float, 0, CV_MAT_ELEM(*randInd, int, j, 0));
      CV_MAT_ELEM(*samplePoints, float, 1, j) =
      CV_MAT_ELEM(*points, float, 1, CV_MAT_ELEM(*randInd, int, j, 0));
    }

    //fit the line
    mcvFitRobustLine(samplePoints, curLineRTheta, curLineAbc);

    //get end points from points in the samplePoints
    minc = 1e5; mind = 1e5; maxc = -1e5; maxd = -1e5;
    for (int j=0; getEndPoints && j<numSamples; ++j)
    {
      //get x & y
      x = CV_MAT_ELEM(*samplePoints, float, 0, j);
      y = CV_MAT_ELEM(*samplePoints, float, 1, j);

      //get the coordinate to work on
      c = y;
      //compare
      if (c>maxc)
      {
        maxc = c;
        maxp = cvPoint2D32f(x, y);
      }
      if (c<minc)
      {
        minc = c;
        minp = cvPoint2D32f(x, y);
      }
    } //for

    // 	fprintf(stderr, "\nminx=%f, miny=%f\n", minp.x, minp.y);
    // 	fprintf(stderr, "maxp=%f, maxy=%f\n", maxp.x, maxp.y);

    //loop on other points and compute distance to the line
    score=0;
    for (int j=0; j<points->cols; j++)
    {
      // 	    //if not already inside
      // 	    if (!CV_MAT_ELEM(*pointIn, char, 0, j))
      // 	    {
        //compute distance to line
        dist = fabs(CV_MAT_ELEM(*points, float, 0, j) * curLineAbc[0] +
        CV_MAT_ELEM(*points, float, 1, j) * curLineAbc[1] + curLineAbc[2]);
        //check distance
        if (dist<=threshold)
        {
          //add this point
          CV_MAT_ELEM(*pointIn, char, 0, j) = 1;
          //update score
          score += cvGetReal2D(image, (int)(CV_MAT_ELEM(*points, float, 1, j)-.5),
                               (int)(CV_MAT_ELEM(*points, float, 0, j)-.5));
        }
        // 	    }
    }

    //check the number of close points and whether to consider this a good fit
    int numClose = cvCountNonZero(pointIn);
    //cout << "numClose=" << numClose << "\n";
    if (numClose >= numGoodFit)
    {
        //get the points included to fit this line
        CvMat *fitPoints = cvCreateMat(2, numClose, CV_32FC1);
        int k=0;
        //loop on points and copy points included
        for (int j=0; j<points->cols; j++)
      if(CV_MAT_ELEM(*pointIn, char, 0, j))
      {
          CV_MAT_ELEM(*fitPoints, float, 0, k) =
        CV_MAT_ELEM(*points, float, 0, j);
          CV_MAT_ELEM(*fitPoints, float, 1, k) =
        CV_MAT_ELEM(*points, float, 1, j);
          k++;

      }

      //fit the line
      mcvFitRobustLine(fitPoints, curLineRTheta, curLineAbc);

      //compute distances to new line
      dist = 0.;
      for (int j=0; j<fitPoints->cols; j++)
      {
        //compute distance to line
        x = CV_MAT_ELEM(*fitPoints, float, 0, j);
        y = CV_MAT_ELEM(*fitPoints, float, 1, j);
        float d = fabs( x * curLineAbc[0] +
        y * curLineAbc[1] +
        curLineAbc[2])
        * cvGetReal2D(image, (int)(y-.5), (int)(x-.5));
        dist += d;
      }

      //now check if we are getting the end points
      if (getEndPoints)
      {

        //get distances
        mind = minp.x * curLineAbc[0] +
        minp.y * curLineAbc[1] + curLineAbc[2];
        maxd = maxp.x * curLineAbc[0] +
        maxp.y * curLineAbc[1] + curLineAbc[2];

        //we have the index of min and max points, and
        //their distance, so just get them and compute
        //the end points
        curEndPointLine.startPoint.x = minp.x
        - mind * curLineAbc[0];
        curEndPointLine.startPoint.y = minp.y
        - mind * curLineAbc[1];

        curEndPointLine.endPoint.x = maxp.x
        - maxd * curLineAbc[0];
        curEndPointLine.endPoint.y = maxp.y
        - maxd * curLineAbc[1];

        // 		SHOW_MAT(fitPoints, "fitPoints");
        //  		SHOW_LINE(curEndPointLine, "line");
      }

      //dist /= score;

      //clear fitPoints
      cvReleaseMat(&fitPoints);

      //check if to keep the line as best
      if (score>=scoreThreshold && score>bestScore)//dist<bestDist //(numClose > bestScore)
      {
        //update max
        bestScore = score; //numClose;
        bestDist = dist;
        //copy
        bestLineRTheta[0] = curLineRTheta[0];
        bestLineRTheta[1] = curLineRTheta[1];
        bestLineAbc[0] = curLineAbc[0];
        bestLineAbc[1] = curLineAbc[1];
        bestLineAbc[2] = curLineAbc[2];
        bestEndPointLine = curEndPointLine;
      }
    } // if numClose
  } // for i

  //return
  if (lineRTheta)
  {
    lineRTheta[0] = bestLineRTheta[0];
    lineRTheta[1] = bestLineRTheta[1];
  }
  if (lineXY)
  {
    if (getEndPoints)
      *lineXY = bestEndPointLine;
    else
      mcvIntersectLineRThetaWithBB(lineRTheta[0], lineRTheta[1],
                                   cvSize(image->cols-1, image->rows-1),
                                   lineXY);
  }
  if (lineScore)
    *lineScore = bestScore;

  //clear
  cvReleaseMat(&points);
  cvReleaseMat(&samplePoints);
  cvReleaseMat(&randInd);
  cvReleaseMat(&pointIn);
}

/** This function sets the matrix to a value except for the mask window passed in
 *
 * \param inMat input matrix
 * \param mask the rectangle defining the mask: (xleft, ytop, width, height)
 * \param val the value to put
 */
void  mcvSetMat(CvMat *inMat, CvRect mask, double val)
{

  //get x-end points of region to work on, and work on the whole image height
  //(int)fmax(fmin(line.startPoint.x, line.endPoint.x)-xwindow, 0);
  int xstart = mask.x, xend = mask.x + mask.width-1;
  //xend = (int)fmin(fmax(line.startPoint.x, line.endPoint.x), width-1);
  int ystart = mask.y, yend = mask.y + mask.height-1;

  //set other two windows to zero
  CvMat maskMat;
  CvRect rect;
  //part to the left of required region
  rect = cvRect(0, 0, xstart-1, inMat->height);
  if (rect.x<inMat->width && rect.y<inMat->height &&
    rect.x>=0 && rect.y>=0 && rect.width>0 && rect.height>0)
  {
    cvGetSubRect(inMat, &maskMat, rect);
    cvSet(&maskMat, cvRealScalar(val));
  }
  //part to the right of required region
  rect = cvRect(xend+1, 0, inMat->width-xend-1, inMat->height);
  if (rect.x<inMat->width && rect.y<inMat->height &&
    rect.x>=0 && rect.y>=0 && rect.width>0 && rect.height>0)
  {
    cvGetSubRect(inMat, &maskMat, rect);
    cvSet(&maskMat, cvRealScalar(val));
  }

  //part to the top
  rect = cvRect(xstart, 0, mask.width, ystart-1);
  if (rect.x<inMat->width && rect.y<inMat->height &&
    rect.x>=0 && rect.y>=0 && rect.width>0 && rect.height>0)
  {
    cvGetSubRect(inMat, &maskMat, rect);
    cvSet(&maskMat, cvRealScalar(val));
  }

  //part to the bottom
  rect = cvRect(xstart, yend+1, mask.width, inMat->height-yend-1);
  if (rect.x<inMat->width && rect.y<inMat->height &&
    rect.x>=0 && rect.y>=0 && rect.width>0 && rect.height>0)
  {
    cvGetSubRect(inMat, &maskMat, rect);
    cvSet(&maskMat, cvRealScalar(val));
  }
}

/** \brief This function groups together bounding boxes
 *
 * \param size the size of image containing the lines
 * \param boxes a vector of output grouped bounding boxes
 * \param groupThreshold the threshold used for grouping (ratio of overlap)
 */
void mcvGroupBoundingBoxes(vector<CvRect> &boxes, float groupThreshold)
{
  bool cont = true;

  //Todo: check if to intersect with bounding box or not

  //save boxes
  //vector<CvRect> tboxes = boxes;

  //loop to get the largest overlap (according to type) and check
  //the overlap ratio
  float overlap, maxOverlap;
  while(cont)
  {
    maxOverlap =  overlap = -1e5;
    //loop on lines and get max overlap
    vector<CvRect>::iterator i, j, maxI, maxJ;
    for(i = boxes.begin(); i != boxes.end(); i++)
    {
      for(j = i+1; j != boxes.end(); j++)
      {
        //get one with smallest y, and compute the y2 - y1 / height of smallest
        //i.e. (y12 - y21) / (y22 - y21)
        overlap = i->y < j->y  ?
        (i->y + i->height - j->y) / (float)j->height :
        (j->y + j->height - i->y) / (float)i->height;

        //get maximum
        if(overlap > maxOverlap)
        {
          maxI = i;
          maxJ = j;
          maxOverlap = overlap;
        }
      } //for j
    } // for i

    //now check the max overlap found against the threshold
    if (maxOverlap >= groupThreshold)
    {
      //combine the two boxes
      *maxI  = cvRect(min((*maxI).x, (*maxJ).x),
                      min((*maxI).y, (*maxJ).y),
                      max((*maxI).width, (*maxJ).width),
                      max((*maxI).height, (*maxJ).height));
                      //delete the second one
                      boxes.erase(maxJ);
    }
    else
      //stop
      cont = false;
  } //while
}

/** \brief This function extracts bounding boxes from lines
 *
 * \param lines vector of lines
 * \param size the size of image containing the lines
 * \param boxes a vector of output bounding boxes
 */
void mcvGetLinesBoundingBoxes(const vector<Line> &lines, CvSize size, vector<CvRect> &boxes)
{
  //copy lines to boxes
  int start, end;
  //clear
  boxes.clear();
  for(unsigned int i=0; i<lines.size(); ++i)
  {
    //get min and max y and add the bounding box covering the whole width
    start = (int)fmin(lines[i].startPoint.y, lines[i].endPoint.y);
    end = (int)fmax(lines[i].startPoint.y, lines[i].endPoint.y);
    boxes.push_back(cvRect(0, start, size.width-1, end-start+1));
  }
}

/** This functions converts a line defined by its two end-points into its
 *   r and theta (origin is at top-left corner with x right and y down and
 * theta measured positive clockwise(with y pointing down) -pi < theta < pi )
 *
 * \param line input line
 * \param r the returned r (normal distance to the line from the origin)
 * \param outLine the output line
 *
 */
void mcvLineXY2RTheta(const Line &line, float &r, float &theta)
{
  //check if vertical line x1==x2
  if(line.startPoint.x == line.endPoint.x)
  {
    //r is the x
    r = fabs(line.startPoint.x);
    //theta is 0 or pi
    theta = line.startPoint.x>=0 ? 0. : CV_PI;
  }
  //check if horizontal i.e. y1==y2
  else if(line.startPoint.y == line.endPoint.y)
  {
    //r is the y
    r = fabs(line.startPoint.y);
    //theta is pi/2 or -pi/2
    theta = (float) line.startPoint.y>=0 ? CV_PI/2 : -CV_PI/2;
  }
  //general line
  else
  {
    //tan(theta) = (x2-x1)/(y1-y2)
    theta =  atan2(line.endPoint.x-line.startPoint.x,
                   line.startPoint.y-line.endPoint.y);
    //r = x*cos(theta)+y*sin(theta)
    float r1 = line.startPoint.x * cos(theta) + line.startPoint.y * sin(theta);
    r = line.endPoint.x * cos(theta) + line.endPoint.y * sin(theta);
    //adjust to add pi if necessary
    if(r1<0 || r<0)
    {
      //add pi
      theta += CV_PI;
      if(theta>CV_PI)
        theta -= 2*CV_PI;
      //take abs
      r = fabs(r);
    }
  }
}

/** This function groups nearby lines
 *
 * \param lines vector of lines
 * \param lineScores scores of input lines
 * \param groupThreshold the threshold used for grouping
 * \param bbox the bounding box to intersect with
 */
void mcvGroupLines(vector<Line> &lines, vector<float> &lineScores,
                   float groupThreshold, CvSize bbox)
{

  //convert the lines into r-theta parameters
  int numInLines = lines.size();
  vector<float> rs(numInLines);
  vector<float> thetas(numInLines);
  for (int i=0; i<numInLines; i++){
    mcvLineXY2RTheta(lines[i], rs[i], thetas[i]);
  }

  //flag for stopping
  bool stop = false;
  while (!stop)
  {
    //minimum distance so far
    float minDist = groupThreshold+5, dist;
    vector<float>::iterator ir, jr, itheta, jtheta, minIr, minJr, minItheta, minJtheta,
    iscore, jscore, minIscore, minJscore;
    //compute pairwise distance between detected maxima
    for (ir=rs.begin(), itheta=thetas.begin(), iscore=lineScores.begin();
    ir!=rs.end(); ir++, itheta++, iscore++)
    for (jr=ir+1, jtheta=itheta+1, jscore=iscore+1;
    jr!=rs.end(); jr++, jtheta++, jscore++)
    {
      //add pi if neg
      float t1 = *itheta<0 ? *itheta : *itheta+CV_PI;
      float t2 = *jtheta<0 ? *jtheta : *jtheta+CV_PI;
      //get distance
      dist = 1 * fabs(*ir - *jr) + 1 * fabs(t1 - t2);//fabs(*itheta - *jtheta);
      //check if minimum
      if (dist<minDist)
      {
        minDist = dist;
        minIr = ir; minItheta = itheta;
        minJr = jr; minJtheta = jtheta;
        minIscore = iscore; minJscore = jscore;
      }
    }
    //check if minimum distance is less than groupThreshold
    if (minDist >= groupThreshold)
      stop = true;
    else
    {
      //put into the first
      *minIr = (*minIr + *minJr)/2;
      *minItheta = (*minItheta + *minJtheta)/2;
      *minIscore = (*minIscore + *minJscore)/2;
      //delete second one
      rs.erase(minJr);
      thetas.erase(minJtheta);
      lineScores.erase(minJscore);
    }
  }//while

  //put back the lines
  lines.clear();
  //lines.resize(rs.size());
  vector<float> newScores=lineScores;
  lineScores.clear();
  for (int i=0; i<(int)rs.size(); i++)
  {
    //get the line
    Line line;
    mcvIntersectLineRThetaWithBB(rs[i], thetas[i], bbox, &line);
    //put in place descendingly
    vector<float>::iterator iscore;
    vector<Line>::iterator iline;
    for (iscore=lineScores.begin(), iline=lines.begin();
    iscore!=lineScores.end() && newScores[i]<=*iscore; iscore++, iline++);
    lineScores.insert(iscore, newScores[i]);
    lines.insert(iline, line);
  }
  //clear
  newScores.clear();
}

/** This fits a parabola to the entered data to get
 * the location of local maximum with sub-pixel accuracy
 *
 * \param val1 first value
 * \param val2 second value
 * \param val3 third value
 *
 * \return the computed location of the local maximum
 */
double mcvGetLocalMaxSubPixel(double val1, double val2, double val3)
{
  //build an array to hold the x-values
  double Xp[] = {1, -1, 1, 0, 0, 1, 1, 1, 1};
  CvMat X = cvMat(3, 3, CV_64FC1, Xp);

  //array to hold the y values
  double yp[] = {val1, val2, val3};
  CvMat y = cvMat(3, 1, CV_64FC1, yp);

  //solve to get the coefficients
  double Ap[3];
  CvMat A = cvMat(3, 1, CV_64FC1, Ap);
  cvSolve(&X, &y, &A, CV_SVD);

  //get the local max
  double max;
  max = -0.5 * Ap[1] / Ap[0];

  //return
  return max;
}

void reconfigureCallback(v_disparity::v_disparity_nodeConfig &config, uint32_t level)
{
    // gaussian blur size
    blur_size = (config.blur_size / 2) * 2 + 1;

    // binarization threshold
    binarization_threshold = config.binarization_threshold;

    // adaptive binarization parameters
    adaptive_block_size = (config.adaptive_block_size / 2) * 2 + 1;
    adaptive_c = config.adaptive_c;

    // erosion parameters
    erosion_size = config.erosion_size;
    erosion_element = config.erosion_element;

    // dilate parameters
    dilate_size = config.dilate_size;
    dilate_element = config.dilate_element;

    // canny parameters
    canny_aperture_size = config.canny_aperture_size;
    canny_threshold1 = config.canny_threshold1;
    canny_threshold2 = config.canny_threshold2;

    // probabilistic hough lines parameters
    hough_rho = config.hough_rho;
//    hough_theta = config.hough_theta;
    hough_threshold = config.hough_threshold;
    hough_min_line_lenght = config.hough_min_line_lenght;
    hough_max_line_gap = config.hough_max_line_gap;

    if(config.disp_start_row >= config.disp_end_row)
        config.disp_start_row = config.disp_end_row - 1;
    if(config.disp_start_col >= config.disp_end_col)
        config.disp_start_col = config.disp_end_col - 1;

    if(config.disp_start_row < 0)
        disp_start_row = 0;
    else if(config.disp_start_row >= camera_calibration->height)
        disp_start_row = camera_calibration->height - 1; // So as to leave at least one pixel
    else
        disp_start_row = config.disp_start_row;

    if(config.disp_end_row < 1)
        disp_end_row = 1; // So as to leave at least one pixel
    else if(config.disp_end_row > camera_calibration->height)
        disp_end_row = camera_calibration->height;
    else
        disp_end_row = config.disp_end_row;

    if(config.disp_start_col < 0)
        disp_start_col = 0;
    else if(config.disp_start_col >= camera_calibration->width)
        disp_start_col = camera_calibration->width - 1; // So as to leave at least one pixel
    else
        disp_start_col = config.disp_start_col;

    if(config.disp_end_col < 1)
        disp_end_col = 1; // So as to leave at least one pixel
    else if(config.disp_end_col > camera_calibration->width)
        disp_end_col = camera_calibration->width;
    else
        disp_end_col = config.disp_end_col;

    config.disp_start_row = disp_start_row;
    config.disp_end_row = disp_end_row;
    config.disp_start_col = disp_start_col;
    config.disp_end_col = disp_end_col;

    min_v_disparity = config.min_v_disparity;

    // v_disparity filtering flags
    perform_blur = config.perform_blur;
    perform_binarization = config.perform_binarization;
    perform_adaptive_binarization = config.perform_adaptive_binarization;
    perform_erosion = config.perform_erosion;
    perform_dilate = config.perform_dilate;
    perform_canny = config.perform_canny;

    // warping rectangle
    warping_lateral_distance = config.warping_lateral_distance;
    warping_forward_min_distance = config.warping_forward_min_distance;
    warping_forward_max_distance = config.warping_forward_max_distance;

    // warp image dimension
    homography_x_resolution = config.homography_x_resolution;
    homography_y_resolution = config.homography_y_resolution;

    // Road lane detector
    lane_width_m = config.lane_width_m;
    lane_height_m = config.lane_height_m;
    kernel_width_px = config.kernel_width_px;
    kernel_height_px = config.kernel_height_px;

    // Road lane detector image-processing
    perform_lane_hough = config.perform_lane_hough;
    perform_lane_normalization = config.perform_lane_normalization;
    perform_lane_blur = config.perform_lane_blur;
    perform_lane_quantile_thresholding = config.perform_lane_quantile_thresholding;
    perform_filter_negative_values = config.perform_filter_negative_values;
    lane_blur_size = (config.lane_blur_size / 2) * 2 + 1;
    lane_thresholding_quantile = config.lane_thresholding_quantile;
    lane_min_angle = config.lane_min_angle;
    lane_max_angle = config.lane_max_angle;

    perform_smooth_sum_lines = config.perform_smooth_sum_lines;
    smooth_sum_lines_size = (config.smooth_sum_lines_size / 2) * 2 + 1;
    lane_score_threshold = config.lane_score_threshold;
    perform_lane_binarization = config.perform_lane_binarization;
    lane_binarization_threshold = config.lane_binarization_threshold;
    lane_local_max_ignore = config.lane_local_max_ignore;
    lane_detection_threshold = config.lane_detection_threshold;
    lane_group_threshold = config.lane_group_threshold;
    lane_group_overlap_threshold = config.lane_group_overlap_threshold;
    lane_ransac_window = config.lane_ransac_window;
}



/**
 * [vec4iAngle description]
 * @param  line   [description]
 * @param  degree whether if the results is in Deg or Rad
 * @return        [description]
 */
double vec4iAngle(cv::Vec4i &line, bool degree) {
    int x1 = line[0]; int y1 = line[1];
    int x2 = line[2]; int y2 = line[3];
    double angle = atan2(y2 - y1, x2 - x1);

    return degree ? angle * (180.0 / CV_PI) : angle;
}

/**
 * @brief getSlopeFromVec4i
 * @param line
 * @return
 */
double getSlopeFromVec4i(const cv::Vec4i& line, bool inverted)
{
    int x1 = line[0]; int y1 = line[1];
    int x2 = line[2]; int y2 = line[3];

    return inverted ? ((double)(x2 - x1) / (double)(y2 - y1)) : ((double)(y2 - y1) / (double)(x2 - x1));
}

/**
 * @param  line [description]
 * @param  K    float64[9] Intrinsic camera matrix for the raw (distorted) images
 * @return      Pitch of camera sensors (rad) [-PI, PI]
 */
double getCamerasAngleFromRoadLine(const cv::Vec4i& line){
    // get calibrations values
    double v_0 = camera_calibration->P[6];
    double f = camera_calibration->P[0];

    // V_or calculation
    // y1 = (slope)*x1 + q
    //  q = y1 - (slope) * x1
    int x1 = line[0]; int y1 = line[1];
    double slope = getSlopeFromVec4i(line, false);
    double v_or = y1 - slope * x1;

    double alpha = f; // a=f/pixel_size (it's assumed that alpha_x is the same of alpha_y)

    return atan2(v_0 - v_or, alpha);
}

/**
 * @param  b     baseline between two cameras (m)
 * @param  theta angle between cameras and road normal (rad)
 * @param  slope angle of the road (rad)
 * @return Height of the camera sensors
 */
double getCamerasHeightFromRoadLine(const cv::Vec4i& line, double b, double theta){
    return b * (cos(theta) / getSlopeFromVec4i(line, true));
}


/**
 * @param theta : angle of camera sensors (rad)
 * @param h : height of camera sensors (m)
 * @return 4x4 trasform matrix
 */
cv::Mat getCameraRototraslation(double theta, double h){
    // homogeneous rotation matrix on X axis by -90° --------------------------------------------------
    cv::Mat R_x = cv::Mat::eye(4, 4, CV_32F);
    double rot = - CV_PI / 2;
    R_x.at<float>(1,1) = cos(rot);
    R_x.at<float>(1,2) = -sin(rot);
    R_x.at<float>(2,1) = sin(rot);
    R_x.at<float>(2,2) = cos(rot);

    // homogeneous rotation matrix on X axis by -theta --------------------------------------------------
    cv::Mat R_theta = cv::Mat::eye(4, 4, CV_32F);
    R_theta.at<float>(1,1) = cos(-theta);
    R_theta.at<float>(1,2) = -sin(-theta);
    R_theta.at<float>(2,1) = sin(-theta);
    R_theta.at<float>(2,2) = cos(-theta);

    // homogeneous rotation matrix on Y axis by 90° ------------------------------------------------------
    cv::Mat R_y = cv::Mat::eye(4, 4, CV_32F);
    rot = CV_PI / 2;
    R_y.at<float>(0,0) = cos(rot);
    R_y.at<float>(0,2) = sin(rot);
    R_y.at<float>(2,0) = -sin(rot);
    R_y.at<float>(2,2) = cos(rot);

    // traslation matrix ---------------------------------------------------------------------------------
    cv::Mat T_h = cv::Mat::eye(4, 4, CV_32F);
    T_h.at<float>(2,3) = h;

    // Rotation matrix
    cv::Mat R = cv::Mat::zeros(4, 4, CV_32F);
    R = R_x * R_y * R_theta;

    // Rototraslation matrix
    cv::Mat RT = cv::Mat::zeros(4, 4, CV_32F);
    return RT = T_h * R;
}

/**
 * @return 3x4 projection matrix from camera calibration data
 */
cv::Mat getProjectionMatrix(){
    cv::Mat P = cv::Mat::zeros(3, 4, CV_32F);
    P.at<float>(0,0) = camera_calibration->P[0];//718.856;
    P.at<float>(0,2) = camera_calibration->P[2];//607.1928;
    P.at<float>(0,3) = camera_calibration->P[3];//-386.1448;
    P.at<float>(1,1) = camera_calibration->P[5];//718.856;
    P.at<float>(1,2) = camera_calibration->P[6];//185.2157;
    P.at<float>(2,2) = 1;
    return P;
}

/**
 * convert ROS image to OpenCV
 */
void imageCb(const sensor_msgs::ImageConstPtr& msg)
{
    cv_ptr_lock.lock();
    try
    {
        cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }
    cv_ptr_lock.unlock();
}

/**
 * @brief disparityCallback
 * @param msg
 */
void disparityCallback(const stereo_msgs::DisparityImage& msg)
{
    // convert ROS msg to OpenCV
    disparity_ptr_lock.lock();
    try
    {
      disparity_cv_ptr = cv_bridge::toCvCopy(msg.image);

    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
    cv::Mat disparity_image = disparity_cv_ptr->image.clone();
//    cv_ptr_lock.lock();
    cv::Mat right_camera_image = cv_ptr->image.clone();
//    cv_ptr_lock.unlock();
    disparity_ptr_lock.unlock();

    // init disparity range (it must be > 0)
    if(msg.min_disparity < 0) return;
    int disparity_range = msg.max_disparity - msg.min_disparity;

    // init v_disparity image data
    cv::Mat v_disp_data = cv::Mat::zeros(disparity_image.rows, disparity_range, CV_8UC1);

    // calculate v_disparity
    for(int row=0; row < disparity_image.rows; row++)
    {
        for(int col=0; col < disparity_image.cols; col++)
        {
            // read disparity image value
            int disp_val = disparity_image.at<float_t>(row, col);

            // increase v_disparity counter
            if(disp_val != -1){
                v_disp_data.at<uint8_t>(row, disp_val) += 1;
            }
        }
    }

    // Normalize values in 0-255
    cv::normalize(v_disp_data, v_disp_data, 0, 255, cv::NORM_MINMAX, CV_8UC1);

    // Apply some gaussian blur
    if(perform_blur){
        cv::GaussianBlur(v_disp_data, v_disp_data, cv::Size( blur_size, blur_size ), 1);
    }

    // Erode
    if(perform_erosion){
        cv::Mat element = cv::getStructuringElement(
                    erosion_element,
                    cv::Size( 2*erosion_size + 1, 2*erosion_size+1 ),
                    cv::Point( erosion_size, erosion_size )
                    );

        cv::erode( v_disp_data, v_disp_data, element );
    }

    // Dilate
    if(perform_dilate){
        cv::Mat element = cv::getStructuringElement(
                    dilate_element,
                    cv::Size( 2*dilate_size + 1, 2*dilate_size+1 ),
                    cv::Point( dilate_size, dilate_size )
                    );

        cv::dilate( v_disp_data, v_disp_data, element );
    }

    // Binarize
    if(perform_binarization){
        cv::threshold(v_disp_data, v_disp_data, binarization_threshold, 255, cv::THRESH_BINARY);
    }

    if(perform_adaptive_binarization){
        cv::adaptiveThreshold(v_disp_data, v_disp_data, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY_INV, adaptive_block_size, adaptive_c);
    }

    // Canny
    if(perform_canny){
        cv::Canny(v_disp_data, v_disp_data, canny_threshold1, canny_threshold2, canny_aperture_size);
    }

    // Find lines
    std::vector<cv::Vec4i> lines;
    cv::HoughLinesP(v_disp_data, lines, hough_rho, hough_theta, hough_threshold, hough_min_line_lenght, hough_max_line_gap);

    // Display single line
    cv::Mat v_disp_data_color; v_disp_data.copyTo(v_disp_data_color);
    cv::cvtColor(v_disp_data, v_disp_data_color, CV_GRAY2BGR);

    // Get the first line which angle is between 75 and 0
    for( size_t i = 0; i < lines.size(); i++ ){
        // find the first line that has angle between min_angle and max_angle
        float angle = vec4iAngle( lines[i], true );
        if( (angle >= 0) && (angle <= 75) )
        {
            // draw line into image
            cv::line(
                v_disp_data_color,
                cv::Point(lines[i][0], lines[i][1]),
                cv::Point(lines[i][2], lines[i][3]),
                cv::Scalar(0,0,255),
                2,
                8
            );

            // calculate camera sensors pitch
            theta = getCamerasAngleFromRoadLine(lines[i]);

            // calculate camera sensors height
            h = getCamerasHeightFromRoadLine(lines[i], msg.T, theta);

            // Line, theta and H found, exit FOR cycle
            break;
        }
    }

    // Hardcode theta&h for lanes detection test purposes
    theta = 0.0225538;
    h = 1.67305;

    // Print theta and H
    std::cout << "Height: " << h << " Theta: " << theta << std::endl;

    // Get Rototraslation matrix from World to Camera (using pin-hole model)
    cv::Mat RT = getCameraRototraslation(theta, h);

    // Get Projection matrix from camera calibration
    cv::Mat P = getProjectionMatrix();

    // define 3d points ------------------------------------------------------------------------
    cv::Mat points_3d = (cv::Mat_<float>(4,4) <<
                        warping_forward_min_distance, warping_forward_min_distance, warping_forward_max_distance, warping_forward_max_distance,
                        warping_lateral_distance,-warping_lateral_distance, -warping_lateral_distance, warping_lateral_distance,
                        0, 0, 0, 0,
                        1, 1, 1, 1);

    // project 3d points into 2d plane
    cv::Mat points_2d = P * RT.inv() * points_3d;

    // normalize 2d points
    for(int i = 0; i < 4; ++i){
        points_2d.col(i) /= points_2d.at<float>(2,i);
    }

    // init points for findHomography
    cv::Mat image_points = cv::Mat::zeros(1, 4, CV_32FC2);
    cv::Mat world_points = cv::Mat::zeros(1, 4, CV_32FC2);

    cv::Mat points_warped = (cv::Mat_<float>(2,4) <<
                                        0, homography_x_resolution,
                                        homography_x_resolution, 0,
                                        homography_y_resolution, homography_y_resolution,
                                        0, 0);

    for(int i = 0; i < 4; ++i)
    {
        image_points.at<cv::Vec2f>(0,i)[0] = points_2d.at<float>(0,i);
        image_points.at<cv::Vec2f>(0,i)[1] = points_2d.at<float>(1,i);

        world_points.at<cv::Vec2f>(0,i)[0] = points_warped.at<float>(0,i);
        world_points.at<cv::Vec2f>(0,i)[1] = points_warped.at<float>(1,i);
    }

    // Find Homography
    cv::Mat H = cv::findHomography(image_points, world_points);

    // Warp perspective
    cv::Mat warp_image;
    cv::cvtColor(right_camera_image, right_camera_image, CV_BGR2GRAY); /* get single channel gray mat */
    cv::warpPerspective(right_camera_image,
                        warp_image,
                        H,
                        cvSize(homography_x_resolution, homography_y_resolution),
                        cv::INTER_NEAREST);


    // -------------------------------------------------------------------------------------------------------------------
    // LANE DETECTION ----------------------------------------------------------------------------------------------------

    float pixel_meter_y_ratio = homography_y_resolution / (warping_forward_max_distance - warping_forward_min_distance); // ~19.444 px/m
    float pixel_meter_x_ratio = homography_x_resolution / (warping_lateral_distance);   // ~71.43 px/m

    // Lane marking width (paper: 3 inches, this: ~20cm = 0.2m)
    float sigma_x = pixel_meter_x_ratio * lane_width_m;

    // Adjusted according to the required height of lane segment to be detected
    // (set to the equivalent of 1m in the IPM image)
    float sigma_y = pixel_meter_y_ratio * lane_height_m;

    // Gaussian filter in Y direction (smooth) ------------------
    cv::Mat y = cv::Mat::zeros(2*kernel_height_px+1, 1, CV_32FC1);
    //get variance
    float sigma_y_2 = sigma_y * sigma_y;
    //get the kernel
    for (double i=-kernel_height_px; i<=kernel_height_px; i++){
        y.at<float_t>(int(i+kernel_height_px), 0) = (float) exp(-(.5/sigma_y_2)*(i*i));
    }
    // -----------------------------------------------------------

    // Second derivative of guassian in X direction --------------
    cv::Mat x = cv::Mat::zeros(2*kernel_width_px+1, 1, CV_32FC1);
    //get variance
    float sigma_x_2 = sigma_x * sigma_x;
    //get the kernel
    for (double i=-kernel_width_px; i<=kernel_width_px; i++){
       x.at<float_t>(int(i+kernel_width_px), 0) = (float)(exp(-.5*i*i)/sigma_x_2 - (i*i)*exp(-(.5/sigma_x_2)*i*i)/(sigma_x_2*sigma_x_2));
    }
    // -----------------------------------------------------------

    //combine the 2D kernel
    cv::Mat kernel = cv::Mat::zeros(2*kernel_height_px+1, 2*kernel_width_px+1, CV_32FC1);
    cv::gemm(y, x, 1, x, 0, kernel, cv::GEMM_2_T);

    //subtract the mean
    cv::Scalar mean = cv::mean(kernel);
    cv::subtract(kernel, mean, kernel);

    //copy warped image to IPM
    cv::Mat ipm = cv::Mat::zeros(warp_image.rows, warp_image.cols, CV_32FC1);
    warp_image.copyTo(ipm);

    //do the filtering
    cv::filter2D(ipm, ipm, CV_32F, kernel);

    // Perform some image processing:
    //      Normalize values in 0-255
    if(perform_lane_normalization){
        cv::normalize(ipm, ipm, 0, 255, cv::NORM_MINMAX, CV_8UC1);
    }

    //      Apply gaussian blur
    if(perform_lane_blur){
        cv::GaussianBlur(ipm, ipm, cv::Size( lane_blur_size, lane_blur_size ), 1);
    }

    //      Zero out negative values
    if(perform_filter_negative_values){
        for (int row=0; row < ipm.rows; row++){
            for (int col=0; col < ipm.cols; col++){
                if ( ipm.at<float_t>(row, col) < 0){
                    ipm.at<float_t>(row, col) = 0.0;
                }
            }
        }
    }

    if(perform_lane_quantile_thresholding){
        // copy image data into horizontal vector
        cv::Mat horizontal_ipm = ipm.clone();
        horizontal_ipm = horizontal_ipm.reshape(0,1);

        // copy image data into a std::vector
        std::vector<float> probs;
        for(int col=0; col < horizontal_ipm.cols; col++){
            probs.push_back(horizontal_ipm.at<float_t>(0, col));
        }

        // compute quantile
        double pos = (probs.size() - 1) * lane_thresholding_quantile;
        uint ind = uint(pos);
        double delta = pos - ind;
        cout << "   size: " << probs.size() << " pos: " << pos << " ind: " << ind << " delta: " << delta << endl;
        std::nth_element(probs.begin(), probs.begin() + ind, probs.end());
        float i1 = probs.at(ind+1);
        float i2 = *std::min_element(probs.begin() + ind + 1, probs.end());
        float qval = (float) (i1 * (1.0 - delta) + i2 * delta);
         cout << "   qtile: " << lane_thresholding_quantile << " qval: " << qval << endl;

        // filter out image values below quantile
        if(qval >= 0.0005 || qval < 1000){
            for (int row=0; row < ipm.rows; row++){
                for (int col=0; col < ipm.cols; col++){
                    if(ipm.at<float_t>(row, col) < qval){
                        ipm.at<float_t>(row, col) = 0.0;
                    }
                }
            }
        }
        else
        {
            cout << "   Quantile thresholding error: unexpected quantile value" << endl;
        }
    }

    if(perform_lane_binarization){
        cv::threshold(ipm, ipm, lane_binarization_threshold, 255, cv::THRESH_BINARY);
    }

    // build sum of pixel values of IPM image
    cv::Mat sum_lines;
    cv::reduce(ipm, sum_lines, 0, CV_REDUCE_SUM, CV_32FC1);
    sum_lines.reshape(0, ipm.cols);

    // Smooth
    if(perform_smooth_sum_lines){
        cv::GaussianBlur(sum_lines, sum_lines, cv::Size(1, smooth_sum_lines_size), 0);
    }

    // build sum lines image to plot
//    cv::normalize(sum_lines, sum_lines, 0, 400, cv::NORM_MINMAX, CV_32FC1);
//    cv::Mat sum_lines_image = cv::Mat::zeros(400, ipm.cols, CV_32FC1);
//    //      set pixel color = 1 at current column value
//    for(int i=0; i<sum_lines.cols; i++){
//        float current_value = sum_lines.at<float_t>(0,i);
//        if(current_value < 0)
//            current_value = 0;
//        if(current_value > 400)
//            current_value = 400;

//        sum_lines_image.at<float_t>(current_value, i) = 1;
//    }


    // Find maximum value of sum_lines vector
    double sum_minVal;
    double sum_maxVal;
    cv::Point sum_minLoc;
    cv::Point sum_maxLoc;
    cv::minMaxLoc( sum_lines, &sum_minVal, &sum_maxVal, &sum_minLoc, &sum_maxLoc);
    cout << "   SUM_LINES:" << endl;
    cout << "       min val : " << sum_minVal << endl;
    cout << "       max val: " << sum_maxVal << endl;

    // Find local maxima of sum_lines vector
    vector<int> sumLinesMaxLoc; // stores locations
    vector<double> sumLinesMax; // stores values
    //loop to get local maxima
    for(int i= 1 + lane_local_max_ignore; i < sum_lines.cols - 1 - lane_local_max_ignore; i++)
    {
        //get that value
        float val = sum_lines.at<float_t>(0,i);

        //check if local maximum
        if( (val > sum_lines.at<float_t>(0,i-1)) && (val > sum_lines.at<float_t>(0,i+1) )
                && (val >= lane_detection_threshold) )
        {
            //iterators for the two vectors
            vector<double>::iterator j;
            vector<int>::iterator k;

            //loop till we find the place to put it in descendingly
            for(j=sumLinesMax.begin(), k=sumLinesMaxLoc.begin();
                j != sumLinesMax.end()  && val<= *j; j++,k++);

            //add its index
            sumLinesMax.insert(j, val);
            sumLinesMaxLoc.insert(k, i);
        }
    }

    //check if didnt find local maxima
    if(sumLinesMax.size()==0 && sum_maxLoc.x>lane_detection_threshold)
    {
      //put maximum
      sumLinesMaxLoc.push_back(sum_maxLoc.x);
      sumLinesMax.push_back(sum_maxVal);
    }

    //process the found maxima
    float maxLineLoc = sum_lines.cols-1;
    vector<Line> lane_lines;  /*returned detected lines (vector of points)*/
    vector<float> lane_lines_scores; /*scores of the detected lines (vector of floats)*/
    for (int i=0; i<(int)sumLinesMax.size(); i++)
    {
        //get subpixel accuracy
        double maxLocAcc =
            mcvGetLocalMaxSubPixel(
                sum_lines.at<float_t>(0, MAX(sumLinesMaxLoc[i]-1,0)),
                sum_lines.at<float_t>(0, sumLinesMaxLoc[i]),
                sum_lines.at<float_t>(0, MIN(sumLinesMaxLoc[i]+1, maxLineLoc))
            );
        maxLocAcc += sumLinesMaxLoc[i];
        maxLocAcc = MIN(MAX(0, maxLocAcc), maxLineLoc);

        //put the extracted line
        Line line;
        line.startPoint.x = (float)maxLocAcc + 0.5;//sumLinesMaxLoc[i]+.5;
        line.startPoint.y = 0.5;
        line.endPoint.x = line.startPoint.x;
        line.endPoint.y = ipm.rows - 0.5;

        lane_lines.push_back(line);
        lane_lines_scores.push_back(sumLinesMax[i]);
    }

    // Performs a RANSAC validation step on the detected lines
    int width = ipm.cols-1;
    int height = ipm.rows-1;
    //  try grouping the lines into regions
    mcvGroupLines(lane_lines, lane_lines_scores, lane_group_threshold, cvSize(width, height));
    //group bounding boxes of lines
    float overlapThreshold = lane_group_overlap_threshold;
    vector<CvRect> boxes;
    mcvGetLinesBoundingBoxes(lane_lines, cvSize(width, height), boxes);
    mcvGroupBoundingBoxes(boxes, overlapThreshold);

    int window = lane_ransac_window;
    vector<Line> newLines;
    vector<float> newScores;
    for (int i=0; i<(int)boxes.size(); i++) //lines
    {
      CvRect mask, box;
      //get box
      box = boxes[i];

      //get extent of window to search in
      //int xstart = (int)fmax(fmin(line.startPoint.x, line.endPoint.x)-window, 0);
      //int xend = (int)fmin(fmax(line.startPoint.x, line.endPoint.x)+window, width-1);
      int xstart = (int)fmax(box.x - window, 0);
      int xend = (int)fmin(box.x + box.width + window, width-1);
      //get the mask
      mask = cvRect(xstart, 0, xend-xstart+1, height);

      //get the subimage to work on
      CvMat sub = ipm.clone();
      CvMat *subimage = cvCloneMat(&sub);

      //clear all but the mask
      mcvSetMat(subimage, mask, 0);

      //get the RANSAC line in this part
      //int numSamples = 5, numIterations = 10, numGoodFit = 15;
      //float threshold = 0.5;
      float lineRTheta[2]={-1,0};
      float lineScore;
      Line line;

//      ransacLineNumSamples = 4
//      ransacLineNumIterations = 40
//      ransacLineThreshold = .2
//      ransacLineScoreThreshold = 0 #4  #6#4#2.5#5#1#1.5#1.5#3#1.5#3 #20 without smoothing and with binarizing
//      ransacLineNumGoodFit = 10 #15#10
//      lineConf->getEndPoints = 0
//          #15 with smoothing and with binarizing: lines
//          #1.5 without binarizing or smoothing: lines
//          #splines: 3 wihtout binarizing or smoothing
//          #splines: 5 with spline length in score
      mcvFitRansacLine(subimage,
                       4,
                       40,
                       0.2,
                       0,
                       10,
                       0,
                       &line, lineRTheta, &lineScore);
      if (lineRTheta[0]>=0)
      {
        bool put =true;
        //make sure it's not horizontal
        if((fabs(lineRTheta[1]) > 20*CV_PI/180))
          put = false;
        if (put)
        {
          newLines.push_back(line);
          newScores.push_back(lineScore);
        }
      } // if

      //clear
      cvReleaseMat(&subimage);
    } // for i

    //update values
    lane_lines.clear();
    lane_lines_scores.clear();
    //mcvGroupLines(newLines, newScores, lineConf->groupThreshold, cvSize(width, height));
    lane_lines = newLines;
    lane_lines_scores = newScores;

    //clean
    boxes.clear();
    newLines.clear();
    newScores.clear();

    // Draw lines
    cv::Mat ipm_color;
    cv::cvtColor(ipm, ipm_color, CV_GRAY2BGR);
    for( size_t i = 0; i < lane_lines.size(); i++ ){
        // draw line into image
        if(lane_lines_scores.at(i) > lane_score_threshold){
            cv::line(
                ipm_color,
                cv::Point( ((Line)lane_lines.at(i)).startPoint.x, ((Line)lane_lines.at(i)).startPoint.y),
                cv::Point( ((Line)lane_lines.at(i)).endPoint.x, ((Line)lane_lines.at(i)).endPoint.y),
                cv::Scalar(0,0,255),
                2,
                8
            );
        }
    }

    // Hough Lines Probabilistic
//    cv::Mat ipm_color;
//    if(perform_lane_hough){
//        // Find lines
//        std::vector<cv::Vec4i> lane_lines;
//        cv::Mat ipm_8uc1;
//        ipm.convertTo(ipm_8uc1,CV_8UC1);
//        cv::HoughLinesP(ipm_8uc1, lane_lines, hough_rho, hough_theta, hough_threshold, hough_min_line_lenght, hough_max_line_gap);

//        // Display single line
//        ipm_8uc1.copyTo(ipm_color);
//        cv::cvtColor(ipm_color, ipm_color, CV_GRAY2BGR);

//        // Get the first line which angle is between 75 and 0
//        for( size_t i = 0; i < lane_lines.size(); i++ ){
//            // find the first line that has angle between min_angle and max_angle
//            float angle = vec4iAngle( lane_lines[i], true );
//            if( (angle >= lane_min_angle) && (angle <= lane_max_angle) )
//            {
//                // draw line into image
//                cv::line(
//                    ipm_color,
//                    cv::Point(lane_lines[i][0], lane_lines[i][1]),
//                    cv::Point(lane_lines[i][2], lane_lines[i][3]),
//                    cv::Scalar(0,0,255),
//                    2,
//                    8
//                );
//            }
//        }
//    }

    // LANE DETECTION END ------------------------------------------------------------------------------------------------
    // -------------------------------------------------------------------------------------------------------------------

    cv::imshow("Vdisparity", v_disp_data_color);
    cv::imshow("RightImage", right_camera_image);
    cv::imshow("RightImageWarped", warp_image);
    cv::imshow("kernel", kernel);
    cv::imshow("FilteredIPM", ipm);
    cv::imshow("IPMwithLines", ipm_color);
//    cv::imshow("LinesSum", sum_lines_image);
//    if(perform_lane_hough){
//        cv::imshow("HoughLinesIPM", ipm_color);
//    }

    // Publish image (v_disparity + red line)
//    cv_bridge::CvImage v_disp_cv_image;
//    sensor_msgs::Image v_disp_msg_image;
//    v_disp_cv_image.encoding = sensor_msgs::image_encodings::BGR8;
//    v_disp_cv_image.header.frame_id = "pippo";
//    v_disp_cv_image.header.stamp = ros::Time::now();
//    v_disp_cv_image.image = v_disp_data_color;
//    v_disp_cv_image.toImageMsg(v_disp_msg_image);
//    publisher.publish(v_disp_msg_image);


//    // Publish image (warped image)
//    cv_bridge::CvImage warp_cv_image;
//    sensor_msgs::Image warp_msg_image;
//    warp_cv_image.encoding = sensor_msgs::image_encodings::MONO8;
//    warp_cv_image.header.frame_id = "pippo";
//    warp_cv_image.header.stamp = ros::Time::now();
//    warp_cv_image.image = warp_image;
//    warp_cv_image.toImageMsg(warp_msg_image);
//    publisher_warp.publish(warp_msg_image);
}

int main(int argc, char *argv[])
{
    // init ROS and NodeHandlecm
    ros::init(argc, argv, "v_disparity_node");
    ros::NodeHandle node_handle("~");

    // save camera calibration
    camera_calibration = ros::topic::waitForMessage<sensor_msgs::CameraInfo>("/kitti_stereo/right/camera_info");

    // init dynamic reconfigure
    dynamic_reconfigure::Server<v_disparity::v_disparity_nodeConfig> server;
    dynamic_reconfigure::Server<v_disparity::v_disparity_nodeConfig>::CallbackType f;
    f = boost::bind(&reconfigureCallback, _1, _2);
    server.setCallback(f);

    // start opencv windows
    cvNamedWindow("RightImage");
    cvNamedWindow("RightImageWarped");
    cvNamedWindow("Vdisparity");
    cvNamedWindow("kernel");
    cvNamedWindow("FilteredIPM");
//    cvNamedWindow("LinesSum");
//    cvNamedWindow("HoughLinesIPM");
    cvNamedWindow("IPMwithLines");
    cvStartWindowThread();

    // publishers
    publisher = node_handle.advertise<sensor_msgs::Image>("/v_disp/v_disp_image", 1);
    publisher_warp = node_handle.advertise<sensor_msgs::Image>("/v_disp/warped_image", 1);

    // show right-camera image
    image_transport::ImageTransport it(node_handle);
    image_transport::Subscriber image_sub_ = it.subscribe("/kitti_stereo/right/image_rect", 1, imageCb);

    // subscribe to kitti disparity image
    ros::Subscriber sub = node_handle.subscribe("/kitti_stereo/disparity", 1, disparityCallback);
//    ros::Subscriber sub = node_handle.subscribe("/stereo/disparity", 1, disparityCallback);

    // spin
    ros::spin();

    return 0;
}
