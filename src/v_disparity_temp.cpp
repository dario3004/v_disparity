#include <algorithm>
#include <ros/ros.h>
#include "stereo_msgs/DisparityImage.h"
#include "sensor_msgs/CameraInfo.h"
#include "sensor_msgs/Image.h"
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>     //make sure to include the relevant headerfiles
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <math.h>

// K: [718.856,   0.000, 607.1928,
//       0.000, 718.856, 185.2157,
//       0.000,   0.000,   1.0000]
double cameras_baseline = 0.54; /// found @kitti setup

// publisher
ros::Publisher publisher_norm;
ros::Publisher publisher_lines;
ros::Publisher publisher_lines_prob;
ros::Publisher publisher_canny_prob;
ros::Publisher publisher_canny;
ros::Publisher publisher_dilate_prob;
ros::Publisher publisher_gauss_norm;
ros::Publisher publisher_dilate_prob_bin;
ros::Publisher publisher_lines_prob_bin;

// stores camera_info
sensor_msgs::CameraInfo camera_info;
ros::Subscriber camera_info_sub;

/**
 * @brief getSlopeFromVec4i
 * @param line
 * @return
 */
double getSlopeFromVec4i(const cv::Vec4i& line)
{
    int x1 = line[0]; int y1 = line[1];
    int x2 = line[2]; int y2 = line[3];
    return ((double)(y2 - y1) / (double)(x2 - x1));
}

/**
 * @param  line [description]
 * @param  K    float64[9] Intrinsic camera matrix for the raw (distorted) images
 * @return      Pitch of camera sensors (rad) [-PI, PI]
 */
double getCamerasAngleFromRoadLine(const cv::Vec4i& line, const sensor_msgs::CameraInfo& cam_info){
    
    // V_or calculation
    // y1 = (slope)*x1 + q
    //  q = y1 - (slope) * x1
    int x1 = line[0]; int y1 = line[1];
    double slope = getSlopeFromVec4i(line);
    double v_or = y1 - slope * x1;

    // Intrinsic camera matrix for the raw (distorted) images:
    //     [fx  0 cx]
    // K = [ 0 fy cy]
    //     [ 0  0  1]
    double v_0 = cam_info.K.at(5);
    double pixel_size = 1;                        // it's assumed that pixel_size is the same on x and y and it is == 1
    double alpha = cam_info.K.at(0) / pixel_size; // it's assumed that alpha_x is the same of alpha_y

    return atan2(v_0 - v_or, alpha);
}

/**
 * @param  b     baseline between two cameras (m)
 * @param  theta angle between cameras and road normal (rad)
 * @param  slope angle of the road (rad)
 * @return Height of the camera sensors
 */
double getCamerasHeightFromRoadLine(const cv::Vec4i& line, double b, double theta){
    return b * (cos(theta) / getSlopeFromVec4i(line));
}

/**
 * [vec4iAngle description]
 * @param  line   [description]
 * @param  degree whether if the results is in Deg or Rad
 * @return        [description]
 */
double vec4iAngle(cv::Vec4i &line, bool degree) {
    int x1 = line[0]; int y1 = line[1];
    int x2 = line[2]; int y2 = line[3];
    double angle = atan2(y2 - y1, x2 - x1);

    return degree ? angle * (180.0 / CV_PI) : angle;
}

/**
 * @brief drawSingleLine
 * @param lines_norm_prob
 * @param v_disp_data_norm_prob_color
 * @param max_angle
 * @param min_angle
 */
void drawSingleLine(std::vector<cv::Vec4i>& lines_norm_prob, cv::Mat& v_disp_data_norm_prob_color, double max_angle, double min_angle)
{
    for( size_t i = 0; i < lines_norm_prob.size(); i++ ){

        // find the first line that has angle between min_angle and max_angle
        float angle = vec4iAngle( lines_norm_prob[i], true );
        if( (angle >= min_angle) && (angle <= max_angle) )
        {
            // draw line into image
            cv::line(
                v_disp_data_norm_prob_color,
                cv::Point(lines_norm_prob[i][0], lines_norm_prob[i][1]),
                cv::Point(lines_norm_prob[i][2], lines_norm_prob[i][3]),
                cv::Scalar(0,0,255),
                1,
                8
            );

            // calculate camera sensors pitch
            double theta = getCamerasAngleFromRoadLine(lines_norm_prob[i], camera_info);

            // calculate camera sensors height
            double h = getCamerasHeightFromRoadLine(lines_norm_prob[i], cameras_baseline, theta);

            std::cout << "Line: " << lines_norm_prob[i] << " Angle: " << angle << " Theta: " << theta << " H: " << h << std::endl;

            break;
        }
    }
}

/**
 * @brief disparityCallback
 * @param msg
 */
void disparityCallback(const stereo_msgs::DisparityImage& msg)
{
//    ROS_INFO_STREAM("DISPARITY IMAGE ARRIVED");
//    ROS_INFO_STREAM("min_disparity: " <<  msg.min_disparity);
//    ROS_INFO_STREAM("max_disparity: " << msg.max_disparity);
//    ROS_INFO_STREAM("image size: " << msg.image.data.size());
//    ROS_INFO_STREAM("image height: " << msg.image.height);
//    ROS_INFO_STREAM("image width: " << msg.image.width);

    int disparity_range = 1+(msg.max_disparity - msg.min_disparity);

    // convert ROS image into OpenCV image
    cv_bridge::CvImagePtr cv_ptr;
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg.image);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

    // init v_disparity image data
    cv::Mat v_disp_data = cv::Mat::zeros(cv_ptr->image.rows, disparity_range, CV_8UC1);


    // calculate v_disparity
    for(int row=0; row < cv_ptr->image.rows; row++)
    {
        int disp_index = 0;
        for(int disparity_value = msg.min_disparity; disparity_value < msg.max_disparity; disparity_value++)
        {
            // avoid looking for 0 values
            if(disparity_value == 0)
            {
                v_disp_data.at<uint8_t>(row, disp_index) = 0;
            }
            else
            {
                // build an std::vector from OpenCV image row
                uint8_t *p = cv_ptr->image.ptr<uint8_t>(row);
                std::vector<uint8_t> vec(p, p + cv_ptr->image.cols);

                // count all the elements with 'value == disparity_value' in the interval [i, i + width)
                v_disp_data.at<uint8_t>(row, disp_index) = std::count(vec.begin(), vec.end(), disparity_value);
            }

            disp_index++;
        }
    }

    // Normalize V-Disparity in 0-255 values
    cv::Mat v_disp_data_norm; v_disp_data.copyTo(v_disp_data_norm);
    cv::normalize(v_disp_data, v_disp_data_norm, 0, 255, cv::NORM_MINMAX, CV_8UC1);

    // Create V-Disparity sensor_mgs::Image
    cv_bridge::CvImage v_disp;
    sensor_msgs::Image v_disp_image;
    v_disp.encoding = sensor_msgs::image_encodings::MONO8;
    v_disp.header.frame_id = msg.header.frame_id;
    v_disp.header.stamp = msg.header.stamp;
    v_disp.image = v_disp_data;
    v_disp.toImageMsg(v_disp_image);

    // Create V-Disparity normalized sensor_mgs::Image ---------------------------------------------------------------------------
    cv_bridge::CvImage v_disp_norm;
    sensor_msgs::Image v_disp_image_norm;
    v_disp_norm.encoding = sensor_msgs::image_encodings::MONO8;
    v_disp_norm.header.frame_id = msg.header.frame_id;
    v_disp_norm.header.stamp = msg.header.stamp;
    v_disp_norm.image = v_disp_data_norm;
    v_disp_norm.toImageMsg(v_disp_image_norm);
    publisher_norm.publish(v_disp_image_norm);

    // Create Image with Hough lines (probabilistic): -------------------
    //          0) Remove some noise
    cv::Mat gauss_dst2; v_disp_data_norm.copyTo(gauss_dst2);
    cv::GaussianBlur(v_disp_data_norm, gauss_dst2, cv::Size(3,3), 1);

    //          1) Find lines
    std::vector<cv::Vec4i> lines_norm_prob;
    cv::HoughLinesP(gauss_dst2, lines_norm_prob, 1, CV_PI/180, 80, 50, 10);

    //          2) init a color image identical to normalized image
    cv::Mat v_disp_data_norm_prob_color; v_disp_data_norm.copyTo(v_disp_data_norm_prob_color);
    cv::cvtColor( gauss_dst2, v_disp_data_norm_prob_color, CV_GRAY2BGR );

    //          3) draw lines on color image
//    for( size_t i = 0; i < lines_norm_prob.size(); i++ )
//    {
//        if( (vec4iAngle(lines_norm_prob[i]) >= 3/4 * CV_PI) && (vec4iAngle(lines_norm_prob[i]) <= CV_PI) )
//        {
//            cv::line( v_disp_data_norm_prob_color, cv::Point(lines_norm_prob[i][0], lines_norm_prob[i][1]),
//                cv::Point(lines_norm_prob[i][2], lines_norm_prob[i][3]), cv::Scalar(0,0,255), 1, 8 );
//        }

//    }


    drawSingleLine(lines_norm_prob, v_disp_data_norm_prob_color, 60, 0);

    //          4) init and populate sensor_msgs::Image
    cv_bridge::CvImage v_disp_norm_prob_color;
    sensor_msgs::Image v_disp_image_norm_prob_color;
    v_disp_norm_prob_color.encoding = sensor_msgs::image_encodings::BGR8;
    v_disp_norm_prob_color.header.frame_id = msg.header.frame_id;
    v_disp_norm_prob_color.header.stamp = msg.header.stamp;
    v_disp_norm_prob_color.image = v_disp_data_norm_prob_color;
    v_disp_norm_prob_color.toImageMsg(v_disp_image_norm_prob_color);
    publisher_lines_prob.publish(v_disp_image_norm_prob_color);

//    // Binarize normalized image -------------------------------------------------------------------------------------
//    cv::Mat norm_bin_dst; gauss_dst2.copyTo(norm_bin_dst);
//    cv::adaptiveThreshold(gauss_dst2, norm_bin_dst, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY_INV, 11, 2);

//    // Find lines
//    std::vector<cv::Vec4i> lines_norm_bin;
//    cv::HoughLinesP(norm_bin_dst, lines_norm_bin, 1, CV_PI/180, 80, 50, 10);

//    // init a color image to draw lines into
//    cv::Mat v_disp_norm_bin; norm_bin_dst.copyTo(v_disp_norm_bin);
//    cv::cvtColor( norm_bin_dst, v_disp_norm_bin, CV_GRAY2BGR );

//    // draw lines on color image
//    for( size_t i = 0; i < lines_norm_bin.size(); i++ )
//    {
//        cv::line(   v_disp_norm_bin,
//                    cv::Point(lines_norm_bin[i][0], lines_norm_bin[i][1]),
//                    cv::Point(lines_norm_bin[i][2], lines_norm_bin[i][3]),
//                    cv::Scalar(0,0,255),
//                    2,
//                    8
//                );
//    }

//    // init and populate sensor_msgs::Image
//    cv_bridge::CvImage norm_dil_bin;
//    sensor_msgs::Image norm_dil_bin_image;
//    norm_dil_bin.encoding = sensor_msgs::image_encodings::BGR8;
//    norm_dil_bin.header.frame_id = msg.header.frame_id;
//    norm_dil_bin.header.stamp = msg.header.stamp;
//    norm_dil_bin.image = v_disp_norm_bin;
//    norm_dil_bin.toImageMsg(norm_dil_bin_image);
//    publisher_lines_prob_bin.publish(norm_dil_bin_image);

//    // Create Image with Hough lines (not probabilistic): -------------------
//    //          1) Find lines
//    std::vector<cv::Vec2f> lines_norm;
//    cv::HoughLines( v_disp_data_norm, lines_norm, 1, CV_PI/180, 100 );

//    //          2) init a color image identical to normalized image
//    cv::Mat v_disp_data_norm_color;
//    v_disp_data_norm.copyTo(v_disp_data_norm_color);
//    cv::cvtColor( v_disp_data_norm, v_disp_data_norm_color, CV_GRAY2BGR );

//    //          3) draw lines on color image
//    for( size_t i = 0; i < lines_norm.size(); i++ )
//    {
//        float rho = lines_norm[i][0];
//        float theta = lines_norm[i][1];
//        double a = cos(theta), b = sin(theta);
//        double x0 = a*rho, y0 = b*rho;
//        cv::Point pt1(cvRound(x0 + 1000*(-b)),
//                  cvRound(y0 + 1000*(a)));
//        cv::Point pt2(cvRound(x0 - 1000*(-b)),
//                  cvRound(y0 - 1000*(a)));
//        cv::line( v_disp_data_norm_color, pt1, pt2, cv::Scalar(0,0,255), 3, 8 );
//    }
//    //          4) init and populate sensor_msgs::Image
//    cv_bridge::CvImage v_disp_norm_color;
//    sensor_msgs::Image v_disp_image_norm_color;
//    v_disp_norm_color.encoding = sensor_msgs::image_encodings::BGR8;
//    v_disp_norm_color.header.frame_id = msg.header.frame_id;
//    v_disp_norm_color.header.stamp = msg.header.stamp;
//    v_disp_norm_color.image = v_disp_data_norm_color;
//    v_disp_norm_color.toImageMsg(v_disp_image_norm_color);
//    publisher_lines.publish(v_disp_image_norm_color);

    // Create Image with Hough lines (erode & dilate): -----------------------------
    // init images
    cv::Mat gauss_dst; v_disp_data_norm.copyTo(gauss_dst);
    cv::Mat erode_dst; v_disp_data_norm.copyTo(erode_dst);
    cv::Mat erode_src; v_disp_data_norm.copyTo(erode_src);
    cv::Mat dilate_dst; v_disp_data_norm.copyTo(dilate_dst);
    cv::Mat v_disp_data_dilate_color; v_disp_data_norm.copyTo(v_disp_data_dilate_color);

    // Remove some noise
    cv::GaussianBlur(erode_src, gauss_dst, cv::Size(3,3), 1);

    // Erode
    int erosion_size = 1;
    cv::Mat element = cv::getStructuringElement( cv::MORPH_RECT,
                                            cv::Size( 2*erosion_size + 1, 2*erosion_size+1 ),
                                            cv::Point( erosion_size, erosion_size ) );
    cv::erode( erode_src, erode_dst, element );

    // Dilate
    int dilation_size = 3;
    cv::Mat dil_element = cv::getStructuringElement( cv::MORPH_RECT,
                                         cv::Size( 2*dilation_size + 1, 2*dilation_size+1 ),
                                         cv::Point( dilation_size, dilation_size ) );
    cv::dilate( erode_dst, dilate_dst, dil_element );


    // Find lines
    std::vector<cv::Vec4i> lines_dilate_prob;
    cv::HoughLinesP(dilate_dst, lines_dilate_prob, 1, CV_PI/180, 80, 25, 10);

    // Init a color image
    cv::cvtColor( dilate_dst, v_disp_data_dilate_color, CV_GRAY2BGR );

    // Draw lines on color image
//    for( size_t i = 0; i < lines_dilate_prob.size(); i++ )
//    {
//        cv::line(   v_disp_data_dilate_color,
//                    cv::Point(lines_dilate_prob[i][0], lines_dilate_prob[i][1]),
//                    cv::Point(lines_dilate_prob[i][2], lines_dilate_prob[i][3]),
//                    cv::Scalar(0,0,255),
//                    1,
//                    8
//                );
//    }

    drawSingleLine(lines_dilate_prob, v_disp_data_dilate_color, 60, 0);

    // init and populate sensor_msgs::Image
    cv_bridge::CvImage v_disp_dilate_prob_color;
    sensor_msgs::Image v_disp_image_dilate_color;
    v_disp_dilate_prob_color.encoding = sensor_msgs::image_encodings::BGR8;
    v_disp_dilate_prob_color.header.frame_id = msg.header.frame_id;
    v_disp_dilate_prob_color.header.stamp = msg.header.stamp;
    v_disp_dilate_prob_color.image = v_disp_data_dilate_color;
    v_disp_dilate_prob_color.toImageMsg(v_disp_image_dilate_color);
    publisher_dilate_prob.publish(v_disp_image_dilate_color);

//    // Binarize dilated image -------------------------------------------------------------------------------------
//    cv::Mat bin_dst; dilate_dst.copyTo(bin_dst);
//    cv::adaptiveThreshold(dilate_dst, bin_dst, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY_INV, 11, 2);

//    // Find lines
//    std::vector<cv::Vec4i> lines_dil_bin;
//    cv::HoughLinesP(bin_dst, lines_dil_bin, 1, CV_PI/180, 80, 50, 10);

//    // init a color image to draw lines into
//    cv::Mat v_disp_dil_bin; dilate_dst.copyTo(v_disp_dil_bin);
//    cv::cvtColor( bin_dst, v_disp_dil_bin, CV_GRAY2BGR );

//    // draw lines on color image
//    for( size_t i = 0; i < lines_dil_bin.size(); i++ )
//    {
//        cv::line(   v_disp_dil_bin,
//                    cv::Point(lines_dil_bin[i][0], lines_dil_bin[i][1]),
//                    cv::Point(lines_dil_bin[i][2], lines_dil_bin[i][3]),
//                    cv::Scalar(0,0,255),
//                    2,
//                    8
//                );
//    }

//    // init and populate sensor_msgs::Image
//    cv_bridge::CvImage dil_bin;
//    sensor_msgs::Image dil_bin_image;
//    dil_bin.encoding = sensor_msgs::image_encodings::BGR8;
//    dil_bin.header.frame_id = msg.header.frame_id;
//    dil_bin.header.stamp = msg.header.stamp;
//    dil_bin.image = v_disp_dil_bin;
//    dil_bin.toImageMsg(dil_bin_image);
//    publisher_dilate_prob_bin.publish(dil_bin_image);


    // Create Canny sensor_mgs::Image ------------------------------------------------------------------------------------------
    //    Smooth (cvSmooth)
    //    Threshold (cvThreshold)
    //    Detect edges (cvCanny)


    // Remove some noise
    cv::Mat gauss_dst3; v_disp_data_norm.copyTo(gauss_dst3);
    cv::Mat gauss_src3; v_disp_data_norm.copyTo(gauss_src3);
    cv::GaussianBlur(gauss_src3, gauss_dst3, cv::Size(3,3), 1);

    // Binarize
    cv::Mat thresh_dst; gauss_dst3.copyTo(thresh_dst);
//    cv::adaptiveThreshold(gauss_dst3, thresh_dst, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY_INV, 11, 2);
    cv::threshold(gauss_dst3, thresh_dst, 50, 255, cv::THRESH_BINARY_INV);

    // Find image edge
    cv::Mat v_disp_data_canny; thresh_dst.copyTo(v_disp_data_canny);
    cv::Canny(thresh_dst, v_disp_data_canny, 50, 200, 3);

    // Dilate
    //cv::dilate( v_disp_data_canny, v_disp_data_canny, dil_element );

//    cv_bridge::CvImage v_disp_canny;
//    sensor_msgs::Image v_disp_image_canny;
//    v_disp_canny.encoding = sensor_msgs::image_encodings::MONO8;
//    v_disp_canny.header.frame_id = msg.header.frame_id;
//    v_disp_canny.header.stamp = msg.header.stamp;
//    v_disp_canny.image = v_disp_data_canny;
//    v_disp_canny.toImageMsg(v_disp_image_canny);

    // Create Image with Hough lines (probabilistic):
    //          1) Find lines
    std::vector<cv::Vec4i> lines;
    cv::HoughLinesP(v_disp_data_canny, lines, 1, CV_PI/180, 80, 50, 10);

    //          2) init a color image identical to 'canny image'
    cv::Mat v_disp_data_canny_color;
    v_disp_data_canny.copyTo(v_disp_data_canny_color);
    cv::cvtColor( v_disp_data_canny, v_disp_data_canny_color, CV_GRAY2BGR );

//    //          3) draw lines on color image
//    for( size_t i = 0; i < lines.size(); i++ )
//    {
//        cv::line( v_disp_data_canny_color, cv::Point(lines[i][0], lines[i][1]),
//            cv::Point(lines[i][2], lines[i][3]), cv::Scalar(0,0,255), 3, 8 );
//    }

    drawSingleLine(lines, v_disp_data_canny_color, 60, 0);

    //          4) init and populate sensor_msgs::Image
    cv_bridge::CvImage v_disp_canny_color;
    sensor_msgs::Image v_disp_image_canny_prob_color;
    v_disp_canny_color.encoding = sensor_msgs::image_encodings::BGR8;
    v_disp_canny_color.header.frame_id = msg.header.frame_id;
    v_disp_canny_color.header.stamp = msg.header.stamp;
    v_disp_canny_color.image = v_disp_data_canny_color;
    v_disp_canny_color.toImageMsg(v_disp_image_canny_prob_color);
    publisher_canny_prob.publish(v_disp_image_canny_prob_color);

//    // Create Image with Hough lines (not probabilistic):
//    //          1) Find lines
//    std::vector<cv::Vec2f> lines2;
//    cv::Mat v_disp_data_canny2;
//    v_disp_data.copyTo(v_disp_data_canny2);
//    cv::HoughLines( v_disp_data_canny2, lines2, 1, CV_PI/180, 100 );

//    //          2) init a color image identical to 'canny image'
//    cv::Mat v_disp_data_canny_color2;
//    v_disp_data_canny.copyTo(v_disp_data_canny_color2);
//    cv::cvtColor( v_disp_data_canny, v_disp_data_canny_color2, CV_GRAY2BGR );

//    //          3) draw lines on color image
//    for( size_t i = 0; i < lines2.size(); i++ )
//    {
//        float rho = lines2[i][0];
//        float theta = lines2[i][1];
//        double a = cos(theta), b = sin(theta);
//        double x0 = a*rho, y0 = b*rho;
//        cv::Point pt1(cvRound(x0 + 1000*(-b)),
//                  cvRound(y0 + 1000*(a)));
//        cv::Point pt2(cvRound(x0 - 1000*(-b)),
//                  cvRound(y0 - 1000*(a)));
//        cv::line( v_disp_data_canny_color2, pt1, pt2, cv::Scalar(0,0,255), 3, 8 );
//    }
//    //          4) init and populate sensor_msgs::Image
//    cv_bridge::CvImage v_disp_canny_color2;
//    sensor_msgs::Image v_disp_image_canny_color;
//    v_disp_canny_color2.encoding = sensor_msgs::image_encodings::BGR8;
//    v_disp_canny_color2.header.frame_id = msg.header.frame_id;
//    v_disp_canny_color2.header.stamp = msg.header.stamp;
//    v_disp_canny_color2.image = v_disp_data_canny_color2;
//    v_disp_canny_color2.toImageMsg(v_disp_image_canny_color);
//    publisher_canny.publish(v_disp_image_canny_color);


//    // V DISPARITY CON FILTRO GAUSSIANO --------------------------------------------------------------
//    // init v_disparity image data
//    cv::Mat v_disp_data_gauss(cv_ptr->image.rows, disparity_range, CV_8UC1);
//    cv::Mat image_gauss; cv_ptr->image.copyTo(image_gauss);
//    cv::Mat image_gauss_dst; image_gauss.copyTo(image_gauss_dst);

//    //remove some noise
//    cv::GaussianBlur(image_gauss, image_gauss_dst, cv::Size(5,5), 1);

//    // calculate v_disparity
//    for(int row=0; row < cv_ptr->image.rows; row++)
//    {
//        int disp_index = 0;
//        for(int disparity_value = msg.min_disparity; disparity_value <= msg.max_disparity; disparity_value++)
//        {
//            // avoid looking for 0 values
//            if(disparity_value == 0)
//            {
//                v_disp_data.at<uint8_t>(row, disp_index) = 0;
//                disp_index++;
//            }
//            else
//            {
//                // build an std::vector from OpenCV image row
//                uint8_t *p = image_gauss_dst.ptr<uint8_t>(row);
//                std::vector<uint8_t> vec(p, p + cv_ptr->image.cols);

//                // count all the elements with 'value == disparity_value' in the interval [i, i + width)
//                v_disp_data_gauss.at<uint8_t>(row, disp_index) = std::count(vec.begin(), vec.end(), disparity_value);

//                disp_index++;
//            }
//        }
//    }

//    // Normalize V-Disparity in 0-255 values
//    cv::Mat data_norm; v_disp_data_gauss.copyTo(data_norm);
//    cv::normalize(v_disp_data_gauss, data_norm, 0, 255, cv::NORM_MINMAX, CV_8UC1);

//    // Create V-Disparity normalized sensor_mgs::Image ---------------------------------------------------------------------------
//    cv_bridge::CvImage gauss_norm;
//    sensor_msgs::Image image_gauss_norm;
//    gauss_norm.encoding = sensor_msgs::image_encodings::MONO8;
//    gauss_norm.header.frame_id = msg.header.frame_id;
//    gauss_norm.header.stamp = msg.header.stamp;
//    gauss_norm.image = data_norm;
//    gauss_norm.toImageMsg(image_gauss_norm);
//    publisher_gauss_norm.publish(image_gauss_norm);

}

/**
 * @brief camerainfoCallback
 * @param cam_info
 */
void camerainfoCallback(const sensor_msgs::CameraInfo& cam_info){
    camera_info = cam_info;
    camera_info_sub.shutdown();

    ROS_INFO_STREAM("CAMERA INFO STORED, SHUTTING DOWN SUBSCRIBER");
}

int main(int argc, char *argv[])
{
    // init ROS and NodeHandlecm
    ros::init(argc, argv, "v_disparity_node");
    ros::NodeHandle node_handle("~");

    // init subscribers
    ros::Subscriber sub = node_handle.subscribe("/kitti_stereo/disparity", 1, disparityCallback);
    camera_info_sub = node_handle.subscribe("/kitti_stereo/right/camera_info", 1, camerainfoCallback);

    // v_disparity un-processed
    publisher_norm = node_handle.advertise<sensor_msgs::Image>("/kitti_stereo/v_disparity_norm", 1);

    // simple normalized image publishers
    publisher_lines = node_handle.advertise<sensor_msgs::Image>("/kitti_stereo/lines_norm", 1);
    publisher_lines_prob = node_handle.advertise<sensor_msgs::Image>("/kitti_stereo/lines_norm_prob", 1);
    publisher_lines_prob_bin = node_handle.advertise<sensor_msgs::Image>("/kitti_stereo/lines_norm_prob_bin", 1);

    // canny publishers
    publisher_canny = node_handle.advertise<sensor_msgs::Image>("/kitti_stereo/lines_canny", 1);
    publisher_canny_prob = node_handle.advertise<sensor_msgs::Image>("/kitti_stereo/lines_canny_prob", 1);

    // erode+dilate image publishers
    publisher_dilate_prob = node_handle.advertise<sensor_msgs::Image>("/kitti_stereo/lines_dilate_prob", 1);
    publisher_dilate_prob_bin = node_handle.advertise<sensor_msgs::Image>("/kitti_stereo/lines_dilate_prob_bin", 1);

    // gaussian filter on disparity image publishers
    publisher_gauss_norm =  node_handle.advertise<sensor_msgs::Image>("/kitti_stereo/gauss_norm", 1);

    ROS_INFO_STREAM("v_disparity_node STARTED, LISTENING TO: " << sub.getTopic());

    ros::spin();
	return 0;
}
